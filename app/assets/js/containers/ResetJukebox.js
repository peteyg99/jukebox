import { connect } from 'react-redux'
import { resetJukebox } from '../actions'
import View from '../components/ResetJukebox'

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {dispatch} = dispatchProps

    return {
        ...ownProps,
        resetJukebox: () => {
            dispatch(resetJukebox())
        }
    }
}

const ResetJukebox = connect(
  null,
  null,
  mergeProps
)(View)

export default ResetJukebox