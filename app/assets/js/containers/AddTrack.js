import { connect } from 'react-redux'
import { addTrack } from '../actions'
import View from '../components/AddTrack'

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {dispatch} = dispatchProps

    return {
        ...ownProps,
        addTrack: (name, link) => {
            dispatch(addTrack(name, link))
        }
    }
}

const AddTrack = connect(
  null,
  null,
  mergeProps
)(View)

export default AddTrack