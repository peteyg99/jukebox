import { connect } from 'react-redux'
import { queueTrack } from '../actions'
import View from '../components/QueueTrack'

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {dispatch} = dispatchProps

    return {
        ...ownProps,
        queueTrack: (trackId) => {
            dispatch(queueTrack(trackId))
        }
    }
}

const QueueTrack = connect(
  null,
  null,
  mergeProps
)(View)

export default QueueTrack