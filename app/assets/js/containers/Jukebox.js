import { connect } from 'react-redux'
import { getJukeboxStore } from '../actions'
import View from '../components/Jukebox'

const mapStateToProps = (state, ownProps) => {
    return {
        savedTracks: state.savedTracks.tracks,
        queuedTracks: state.queuedTracks.tracks,
        nowPlaying: state.nowPlaying.track
    }
}

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {dispatch} = dispatchProps

    return {
        ...stateProps,
        willMount: () => {
            dispatch(getJukeboxStore())
        }
    }
}

const Jukebox = connect(
  mapStateToProps,
  null,
  mergeProps
)(View)

export default Jukebox