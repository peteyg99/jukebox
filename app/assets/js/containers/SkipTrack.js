import { connect } from 'react-redux'
import { skipTrack } from '../actions'
import View from '../components/SkipTrack'

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {dispatch} = dispatchProps

    return {
        ...ownProps,
        skipTrack: () => {
            dispatch(skipTrack())
        }
    }
}

const SkipTrack = connect(
  null,
  null,
  mergeProps
)(View)

export default SkipTrack