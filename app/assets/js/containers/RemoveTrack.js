import { connect } from 'react-redux'
import { removeTrack } from '../actions'
import View from '../components/RemoveTrack'

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {dispatch} = dispatchProps

    return {
        ...ownProps,
        removeTrack: (trackId) => {
            dispatch(removeTrack(trackId))
        }
    }
}

const RemoveTrack = connect(
  null,
  null,
  mergeProps
)(View)

export default RemoveTrack