import React, { PropTypes } from 'react'

export default class TrackLogo extends React.Component {
    render(){
        return (
            <img src={this.props.logo} width="25" height="25" />
        )
    }
}

TrackLogo.propTypes = {
  logo: PropTypes.string.isRequired,
}
