import React from 'react'
import ElementHeader from '../components/ElementHeader'
import SkipTrack from '../containers/SkipTrack'
import Track from '../components/Track'

export default class NowPlaying extends React.Component {

    _getEmbed(){
        if (!this.props.track) 
            return (
                <h4 className="text-muted text-center">
                    No track loaded
                </h4>
            )
        
        return (
            <iframe src={this.props.track.embedUrl}
                width="100%" height="380" frameBorder="0" allowTransparency="true"></iframe>
        )
    }

    _getFooter(){
        if (!this.props.track) 
            return (
                <h5 className="text-muted">No track loaded</h5>
            )
        
        return (
            <div className="row">
                <div className="col-sm-8">
                    <Track track={this.props.track} />
                </div>
                <div className="col-sm-4">
                    <SkipTrack />
                </div>
            </div>
        )
    }

    render(){
        return (
            <div className="panel panel-default">
                <ElementHeader headline="Now playing" />
                <div className="panel-body">
                    {this._getEmbed()}
                </div>
                <div className="panel-footer">
                    {this._getFooter()}
                </div>
            </div>
        )
    }

}