import React from 'react'
import SavedTracks from '../components/SavedTracks'
import QueuedTracks from '../components/QueuedTracks'
import NowPlaying from '../components/NowPlaying'
import ResetJukebox from '../containers/ResetJukebox'
import AddTrack from '../containers/AddTrack'

export default class Jukebox extends React.Component {

    componentWillMount() {
        this.props.willMount()
    }

    render(){
        return (
            <div className="row">
                <div className="col-sm-6">
                    <NowPlaying track={this.props.nowPlaying} />
                    <AddTrack />
                </div>
                <div className="col-sm-6">
                    <SavedTracks tracks={this.props.savedTracks} />
                    <QueuedTracks tracks={this.props.queuedTracks} />
                    <ResetJukebox />
                </div>
            </div>
        )
    }
}