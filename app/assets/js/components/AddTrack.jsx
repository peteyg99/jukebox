import React, { PropTypes } from 'react'
import ElementHeader from '../components/ElementHeader'

export default class AddTrack extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            link: ''
        }
    }

    _handleChange(inp, evt) {
        var change = {};
        change[inp] = evt.target.value;
        this.setState(change)
    }

    _handleSubmit(evt) {
        evt.preventDefault()
        this.props.addTrack(this.state.name, this.state.link)
        this.setState({
            name: '',
            link: ''
        })
    }

    render() {
        return (
            <div className="panel panel-default">
                <ElementHeader headline="Add new track" />
                <div className="panel-body">
                    <form onSubmit={this._handleSubmit.bind(this)} className="form-horizontal">

                        <div className="form-group">
                            <label for="trackName" className="control-label col-sm-3">Track name</label>
                            <div className="col-sm-9">
                                <input required value={this.state.name} onChange={this._handleChange.bind(this, 'name')} 
                                    type="text" className="form-control" id="trackName" placeholder="Track name" />
                            </div>
                        </div>
                        <div className="form-group">
                            <label for="trackLink" className="control-label col-sm-3">Track link</label>
                            <div className="col-sm-9">
                                <input required value={this.state.link} onChange={this._handleChange.bind(this, 'link')} 
                                    type="text" className="form-control" id="trackLink" placeholder="eg. https://soundcloud.com/stevenmoove/a-little-soul-smoove-suonho" />
                                <span className="help-block">Add Spotify or Soundcloud tracks.</span>
                            </div>  
                        </div>  
                        <div className="form-group">
                            <div className="col-sm-offset-3 col-sm-9">
                                <button type="submit" className="btn btn-default">Add track</button>
                            </div>  
                        </div>  
                    </form>                
                </div>
            </div>
        )
    }
}