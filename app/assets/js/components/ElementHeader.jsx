import React, { PropTypes } from 'react'

class ElementHeader extends React.Component {
    render(){
        return (
            <div className="panel-heading">
                <h4>{this.props.headline}</h4>
            </div>
        )
    }
}

ElementHeader.propTypes = {
  headline: PropTypes.string.isRequired,
}

export default ElementHeader