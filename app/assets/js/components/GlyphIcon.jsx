import React, { PropTypes } from 'react'

class GlyphIcon extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const classVal = "glyphicon glyphicon-" + this.props.type
        return (
            <span className={classVal}></span>
        )
    }
}

GlyphIcon.propTypes = {
  type: PropTypes.string.isRequired,
}

export default GlyphIcon