import React, { PropTypes } from 'react'
import GlyphIcon from '../components/GlyphIcon'

class RemoveTrack extends React.Component {
    constructor(props) {
        super(props)
    }

    _removeTrack() {
        this.props.removeTrack(this.props.trackId)
    }

    render() {
        return (
            <button className="btn btn-danger btn-block" type="button" onClick={this._removeTrack.bind(this)}>
                <GlyphIcon type="trash" />
            </button>
        )
    }
}

RemoveTrack.propTypes = {
  trackId: PropTypes.number.isRequired,
}

export default RemoveTrack