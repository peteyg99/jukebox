import React from 'react'
import Jukebox from '../containers/Jukebox'

export default class App extends React.Component {
    render(){
        return (
            <div>
                <Jukebox />                
            </div>
        )
    }
}