import React, { PropTypes } from 'react'

export default class SkipTrack extends React.Component {
    constructor(props) {
        super(props)
    }

    _skipTrack() {
        this.props.skipTrack()
    }

    render() {
        return (
            <button className="btn btn-primary btn-block" type="button" onClick={this._skipTrack.bind(this)}>
                Skip track
            </button>
        )
    }
}