import React, { PropTypes } from 'react'
import ElementHeader from '../components/ElementHeader'
import Tracklist from '../components/Tracklist'

class SavedTracks extends React.Component {
    _getTrackList() {
        if (this.props.tracks.length < 1)
            return (
                <h4 className="text-muted text-center">
                    No tracks
                </h4>
            )
        
        return (
            <Tracklist tracks={this.props.tracks} showQueue="true" showRemove="true" />
        )
    }

    render(){
        return (
            <div className="panel panel-default">
                <ElementHeader headline="Saved tracks" />
                <div className="panel-body">
                    {this._getTrackList()}
                </div>
            </div>
        )
    }
}

SavedTracks.defaultProps = {
  tracks: []
};

SavedTracks.propTypes = {
  tracks: PropTypes.array.isRequired,
}

export default SavedTracks