import React, { PropTypes } from 'react'
import TrackLogo from '../components/TrackLogo'
import QueueTrack from '../containers/QueueTrack'
import RemoveTrack from '../containers/RemoveTrack'

class Track extends React.Component {
    constructor(props) {
        super(props)
    }

    _showQueue() {
        if (this.props.showQueue) 
            return <QueueTrack trackId={this.props.track.trackId} />
    }

    _showRemove() {
        if (this.props.showRemove) 
            return <RemoveTrack trackId={this.props.track.trackId} />
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-1">
                    <TrackLogo logo={this.props.track.logo} />
                </div>
                <div className="col-sm-7 text-center">
                    <h5>{this.props.track.name}</h5>
                </div>
                <div className="col-md-2">
                    {this._showQueue()}
                </div>
                <div className="col-md-2">
                    {this._showRemove()}
                </div>
            </div>
        )
    }
}


Track.defaultProps = {
  showQueue: false,
  showRemove: false
}

Track.propTypes = {
    track:  PropTypes.object.isRequired,
}

export default Track