import React, { PropTypes } from 'react'

export default class ResetJukebox extends React.Component {
    constructor(props) {
        super(props)
    }

    _resetJukebox() {
        this.props.resetJukebox()
    }

    render() {
        return (
            <button className="btn btn-danger btn-block" type="button" onClick={this._resetJukebox.bind(this)}>
                Reset Jukebox
            </button>
        )
    }
}
