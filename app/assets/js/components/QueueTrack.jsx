import React, { PropTypes } from 'react'
import GlyphIcon from '../components/GlyphIcon'

class QueueTrack extends React.Component {
    constructor(props) {
        super(props)
    }

    _queueTrack() {
        this.props.queueTrack(this.props.trackId)
    }

    render() {
        return (
            <button className="btn btn-default btn-block" type="button" onClick={this._queueTrack.bind(this)}>
                <GlyphIcon type="plus" />
            </button>
        )
    }
}

QueueTrack.propTypes = {
  trackId: PropTypes.number.isRequired,
}

export default QueueTrack