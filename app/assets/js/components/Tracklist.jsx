import React, { PropTypes } from 'react'
import Track from '../components/Track'

export default class Tracklist extends React.Component {

    render(){
        return (
            <div>
                {this.props.tracks.map((track, index) => (
                    <Track key={index} track={track} showQueue={this.props.showQueue} showRemove={this.props.showRemove} />
                ))}
            </div>
        )
    }
}

Tracklist.defaultProps = {
    showQueue: false,
    showRemove: false,
    tracks: []
}

Tracklist.propTypes = {
  tracks: PropTypes.array.isRequired
}