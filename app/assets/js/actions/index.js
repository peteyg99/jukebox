import { UPDATE_JUKEBOX, UPDATE_JUKEBOX_ERROR } from '../constants'
import axios from 'axios'


export const getJukeboxStore = () => {
    return (dispatch) => {

        axios.get('/?controller=api&action=get-jukebox-state')
            .then(function (response) {
                if (!response.status == 200)
                {
                    return dispatch({ 
                        type: UPDATE_JUKEBOX_ERROR
                    })
                }
                const data = response.data.data
                dispatch({ 
                    type: UPDATE_JUKEBOX,
                    savedTracks: data.savedTracks,
                    queuedTracks: data.queuedTracks,
                    nowPlaying: data.nowPlaying
                })
            })
    }
}

export const queueTrack = (trackId) => {
    return (dispatch) => {
        axios.get('/?controller=api&action=queue-track&trackId='+trackId)
            .then(function (response) {
                if (!response.status == 200)
                {
                    return dispatch({ 
                        type: UPDATE_JUKEBOX_ERROR
                    })
                }
                dispatch(getJukeboxStore())
            })
    }
}

export const skipTrack = () => {
    return (dispatch) => {
        axios.get('/?controller=api&action=skip-track')
            .then(function (response) {
                if (!response.status == 200)
                {
                    return dispatch({ 
                        type: UPDATE_JUKEBOX_ERROR
                    })
                }
                dispatch(getJukeboxStore())
            })
    }
}


export const resetJukebox = () => {
    return (dispatch) => {
        axios.get('/?controller=api&action=reset')
            .then(function (response) {
                if (!response.status == 200)
                {
                    return dispatch({ 
                        type: UPDATE_JUKEBOX_ERROR
                    })
                }
                dispatch(getJukeboxStore())
            })
    }
}

export const addTrack = (trackName, trackLink) => {
    return (dispatch) => {
        axios.get('/?controller=api&action=add-track&trackName='+trackName+'&trackLink='+trackLink)
            .then(function (response) {
                if (!response.status == 200)
                {
                    return dispatch({ 
                        type: UPDATE_JUKEBOX_ERROR
                    })
                }
                dispatch(getJukeboxStore())
            })
    }
}

export const removeTrack = (trackId) => {
    return (dispatch) => {
        axios.get('/?controller=api&action=removeTrack&trackId='+trackId)
            .then(function (response) {
                if (!response.status == 200)
                {
                    return dispatch({ 
                        type: UPDATE_JUKEBOX_ERROR
                    })
                }
                dispatch(getJukeboxStore())
            })
    }
}
