import { UPDATE_JUKEBOX } from '../constants'

export default function SavedTracksReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_JUKEBOX:
            return {
                ...state,
                tracks: action.savedTracks
            }
        default:
            return state
    }
}