import { UPDATE_JUKEBOX } from '../constants'

export default function QueuedTracksReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_JUKEBOX:
            return {
                ...state,
                tracks: action.queuedTracks
            }
        default:
            return state
    }
}