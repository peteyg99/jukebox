import { UPDATE_JUKEBOX } from '../constants'

export default function NowPlayingReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_JUKEBOX:
            return {
                ...state,
                track: action.nowPlaying
            }
        default:
            return state
    }
}