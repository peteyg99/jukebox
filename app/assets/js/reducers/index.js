import { combineReducers } from 'redux'

// Reducers
import SavedTracksReducer from './SavedTracksReducer'
import QueuedTracksReducer from './QueuedTracksReducer'
import NowPlayingReducer from './NowPlayingReducer'

// Combine Reducers
export default combineReducers({
    savedTracks: SavedTracksReducer,
    queuedTracks: QueuedTracksReducer,
    nowPlaying: NowPlayingReducer
})