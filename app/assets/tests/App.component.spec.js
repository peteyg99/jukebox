import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import App from '../js/components/App'
import Jukebox from '../js/containers/Jukebox'

describe('<App /> component', () => {
    it('should render 1 <Jukebox>', function () {
        const enzymeWrapper = shallow(<App />)
        expect(enzymeWrapper.find(Jukebox)).to.have.length(1);
    });
});