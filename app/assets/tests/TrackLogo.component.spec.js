import React from 'react';
import { mount } from 'enzyme';
import { expect, assert } from 'chai';

import { errorOnError } from './helpers/errorOnError'
import TrackLogo from '../js/components/TrackLogo'

function setup(props) {

    // make prop errors throw an error
    const error = console.error;
    console.error = errorOnError()

    const enzymeWrapper = mount(<TrackLogo {...props} />)

    return {
        props,
        enzymeWrapper
    }
}

describe('<TrackLogo /> component', () => {
    it('should set a logo from prop', function () {
        let logo = "test"
        const { enzymeWrapper } = setup({logo})
        
        expect(enzymeWrapper.props().logo).to.be.a('string')
        expect(enzymeWrapper.props().logo).to.equal(logo)
    });

    it('should throw error if no logo', function () {
        assert.throws(function(){
            setup({ logo: null })
        }, Error)
    })

});