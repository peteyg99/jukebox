import React from 'react';
import { mount } from 'enzyme';
import { expect, assert } from 'chai';

import { errorOnError } from './helpers/errorOnError'
import ElementHeader from '../js/components/ElementHeader'

function setup(props) {

    // make prop errors throw an error
    const error = console.error;
    console.error = errorOnError()

    const enzymeWrapper = mount(<ElementHeader {...props} />)

    return {
        props,
        enzymeWrapper
    }
}

describe('<ElementHeader /> component', () => {
    it('should set a headline from prop', function () {
        let headline = "test"
        const { enzymeWrapper } = setup({headline})
        
        expect(enzymeWrapper.props().headline).to.be.a('string')
        expect(enzymeWrapper.props().headline).to.equal(headline)
    });

    it('should throw error if no headline', function () {
        assert.throws(function(){
            setup({ headline: null })
        }, Error)
    })

});