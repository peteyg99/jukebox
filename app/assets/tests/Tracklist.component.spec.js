import React from 'react';
import { mount, shallow } from 'enzyme';
import { expect, assert } from 'chai';

import { errorOnError } from './helpers/errorOnError'
import ElementHeader from '../js/components/ElementHeader'
import Tracklist from '../js/components/Tracklist'
import Track from '../js/components/Track'
import { getTrack } from './helpers/getTracks'

function setup(props) {

    // make prop errors throw an error
    const error = console.error;
    console.error = errorOnError()

    const enzymeWrapper = mount(<Tracklist {...props} />)

    return {
        props,
        enzymeWrapper
    }
}


function getTracks() {
    return [getTrack(), getTrack()]
}

describe('<Tracklist /> component', () => {
    it('should set tracks array from prop', function () {

        const tracks = getTracks()
        const { enzymeWrapper } = setup({tracks})
        
        expect(enzymeWrapper.props().tracks).to.be.a('array')
        expect(enzymeWrapper.props().tracks).to.equal(tracks)
    });

    it('should set tracks a <Track /> for every track prop', function () {

        const tracks = getTracks()
        const { enzymeWrapper } = setup({tracks})
        
        expect(enzymeWrapper.find(Track)).to.have.length(tracks.length);
    });

    it('should throw error if null tracks', function () {
        assert.throws(function(){
            setup({ tracks: null })
        }, Error)
    })

    it('should have default tracks prop', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.props().tracks).to.be.a('array')
    })

});
