import React from 'react';
import { mount } from 'enzyme';
import { expect, assert } from 'chai';

import { errorOnError } from './helpers/errorOnError'
import GlyphIcon from '../js/components/GlyphIcon'

function setup(props) {

    // make prop errors throw an error
    const error = console.error;
    console.error = errorOnError()

    const enzymeWrapper = mount(<GlyphIcon {...props} />)

    return {
        props,
        enzymeWrapper
    }
}

describe('<GlyphIcon /> component', () => {
    it('should set a type from prop', function () {
        let type = "test"
        const { enzymeWrapper } = setup({type})
        
        expect(enzymeWrapper.props().type).to.be.a('string')
        expect(enzymeWrapper.props().type).to.equal(type)
    });

    it('should set a class containing type prop', function () {
        let type = "test"
        const { enzymeWrapper } = setup({type})
        
        expect(enzymeWrapper.find('.glyphicon').props().className).to.contain(type)
    });

    it('should throw error if no type', function () {
        assert.throws(function(){
            setup({ type: null })
        }, Error)
    })

});