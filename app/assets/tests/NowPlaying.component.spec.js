import React from 'react';
import { shallow } from 'enzyme';
import { expect, assert } from 'chai';

import NowPlaying from '../js/components/NowPlaying'
import ElementHeader from '../js/components/ElementHeader'
import Track from '../js/components/Track'
import { getTrack } from './helpers/getTracks'

describe('<NowPlaying /> component', () => {
    it('should render 1 <ElementHeader>', function () {
        const enzymeWrapper = shallow(<NowPlaying />)
        expect(enzymeWrapper.find(ElementHeader)).to.have.length(1);
    });

    it('should render 1 <Track> if track prop set', function () {
        const track = getTrack()
        const enzymeWrapper = shallow(<NowPlaying track={track} />)
        expect(enzymeWrapper.find(Track)).to.have.length(1);
    });

    it('should render 0 <Track> if no track prop set', function () {
        const enzymeWrapper = shallow(<NowPlaying />)
        expect(enzymeWrapper.find(Track)).to.have.length(0);
    });

});