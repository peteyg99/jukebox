import React from 'react';
import { mount } from 'enzyme';
import { expect, assert } from 'chai';

import { errorOnError } from './helpers/errorOnError'
import Track from '../js/components/Track'
import TrackLogo from '../js/components/TrackLogo'
import { getTrack } from './helpers/getTracks'

function setup(testProps) {

    // make prop errors throw an error
    const error = console.error;
    console.error = errorOnError()

    let track = getTrack()
    const props = {
        track,
        ...testProps
    }

    const enzymeWrapper = mount(<Track {...props} />)

    return {
        props,
        enzymeWrapper
    }
}

describe('<Track /> component', () => {
    it('should set a track from prop', function () {
        let track = getTrack()
        track.name = "updated test"
        const { enzymeWrapper } = setup({track})
        
        expect(enzymeWrapper.props().track).to.be.a('object')
        expect(enzymeWrapper.props().track).to.equal(track)
    });

    it('should throw error if no track', function () {
        assert.throws(function(){
            setup({ track: null })
        }, Error)
    })

    it('should render 1 <TrackLogo>', function () {
        const { enzymeWrapper } = setup()
        expect(enzymeWrapper.find(TrackLogo)).to.have.length(1);
    });
    

});