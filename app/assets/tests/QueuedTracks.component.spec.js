import React from 'react';
import { shallow } from 'enzyme';
import { expect, assert } from 'chai';

import QueuedTracks from '../js/components/QueuedTracks'
import ElementHeader from '../js/components/ElementHeader'
import Tracklist from '../js/components/Tracklist'
import { getTrack } from './helpers/getTracks'

describe('<QueuedTracks /> component', () => {
    it('should render 1 <ElementHeader>', function () {
        const enzymeWrapper = shallow(<QueuedTracks />)
        expect(enzymeWrapper.find(ElementHeader)).to.have.length(1);
    });

    it('should render 1 <Tracklist> if track prop set', function () {
        const track = getTrack()
        const enzymeWrapper = shallow(<QueuedTracks tracks={[track]} />)
        expect(enzymeWrapper.find(Tracklist)).to.have.length(1);
    });

    it('should render 0 <Tracklist> if no track prop set', function () {
        const enzymeWrapper = shallow(<QueuedTracks />)
        expect(enzymeWrapper.find(Tracklist)).to.have.length(0);
    });

});