export const errorOnError = () => {
    return (warning, ...args) => {
        if (/(Invalid prop|Failed prop type)/.test(warning)) {
            throw new Error(warning)
        }
        error.apply(console, [warning, ...args])
    }
}