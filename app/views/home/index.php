<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Jukebox</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    </head>

    <body>

        <nav class="navbar navbar-default">
            <div class="container">
                <a class="navbar-brand" href="">Jukebox</a>
            </div>
        </nav>
        
        <div class="container">

            <div id="root"></div>

        </div>

        <script src="/js/client.js"></script>
    </body>
</html>