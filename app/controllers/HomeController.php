<?php 

class HomeController extends BaseController
{
    /**
     * Create a new JukeboxApiController instance.
     *
     * @param array $config
     * @return void
     */
    function __construct($config) 
    {
        try 
        {
            parent::__construct($config);
        }
        catch (Exception $ex)
        {
            self::processError($ex);
        }
    }


    /**
     * Default action.
     *
     * @return void
     */
    public function index() 
    {
        try 
        {
            // Get together variables to pass to view
            $pageVars = (object) array(
            );

            // Show view
            $this->view('home/index', $pageVars);
        }
        catch (Exception $ex)
        {
            self::processError($ex);
        }
    }

    /**
     * Room to implement extra error handling here,
     * currently just redirecting.
     * 
     * @return void
     */
    public static function processError($ex)
    {
        $this->doRedirect('error');
    }
}
