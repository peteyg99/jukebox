<?php 

class JukeboxApiController extends BaseController
{
    /**
     * Create a new JukeboxApiController instance.
     *
     * @param array $config
     * @return void
     */
    function __construct($config) 
    {
        try 
        {
            parent::__construct($config);
    
            // Start session
            session_start();

            // Get/create jukebox
            $this->jukebox = $this->getJukebox();
        }
        catch (Exception $ex)
        {
            self::processError($ex);
            die();
        }
    }

    /**
     * API Endpoint - Get current time.
     *
     * @return json
     */
    public function getTime()
    {
        $timeDisplay = Jukebox::getTimeDisplay();
        self::jsonResponse(true, $timeDisplay);
    }

    /**
     * API Endpoint - Get Jukebox author.
     *
     * @return json
     */
    public function getAuthor()
    {
        $author = Jukebox::getAuthor();
        self::jsonResponse(true, $author);
    }

    /**
     * List tracks.
     *
     * @return json
     */
    private function getJsonListTracks()
    {
        $tracks = $this->jukebox->listTracks();
        return self::getJsonTracks($tracks);
    }

    /**
     * API Endpoint - List tracks.
     *
     * @return json
     */
    public function listTracks()
    {
        $trackData = $this->getJsonListTracks();
        self::jsonResponse(true, $trackData);
    }

    /**
     * API Endpoint - Add new track.
     *
     * @return json
     */
    public function addTrack()
    {
        $trackName = $_GET["trackName"];
        $trackLink = $_GET["trackLink"];
        $trackType = null;
        if (strstr($trackLink, 'spotify')) $trackType = 'spotify';
        if (strstr($trackLink, 'soundcloud')) $trackType = 'soundcloud';

        switch ($trackType)
        {
            case 'spotify':
                $track = new SpotifyTrack($trackName, $trackLink);
                break;
            case 'soundcloud':
                $track = new SoundcloudTrack($trackName, $trackLink);
                break;
            default:
                throw new Exception("Invalid track type");
        }
        $this->jukebox->addTrack($track);
        self::jsonResponse(true);
    }

    /**
     * Get queued track.
     *
     * @return json
     */
    public function removeTrack()
    {
        $trackId = $_GET["trackId"];
        $this->jukebox->removeTrack($trackId);
        self::jsonResponse(true);
    }

    /**
     * API Endpoint - Get queued track.
     *
     * @return json
     */
    public function queueTrack()
    {
        $trackId = $_GET["trackId"];
        $this->jukebox->queueTrack($trackId);
        self::jsonResponse(true);
    }

    /**
     * Get queued tracks.
     *
     * @return array
     */
    private function getJsonQueuedTracks()
    {
        $tracks = $this->jukebox->getQueuedTracks();
        array_shift($tracks);
        return self::getJsonTracks($tracks);
    }

    /**
     * API Endpoint - Get queued tracks.
     *
     * @return json
     */
    public function queuedTracks()
    {
        $trackData = $this->getJsonQueuedTracks();
        self::jsonResponse(true, $trackData);
    }

    /**
     * Get current track info.
     *
     * @return array
     */
    private function getJsonLoadedTrack()
    {
        $track = $this->jukebox->getLoadedTrack();
        return $track ? $track->getTrackViewModel() : null;
    }

    /**
     * API Endpoint - Get current track info.
     *
     * @return json
     */
    public function loadedTrack()
    {
        $trackData = $this->getJsonLoadedTrack();
        self::jsonResponse(true, $trackData);
    }

    /**
     * API Endpoint - Skip to next track.
     *
     * @return json
     */
    public function skipTrack()
    {
        $this->jukebox->skipToNextQueuedTrack();
        self::jsonResponse(true);
    }

    /**
     * API Endpoint - Get full Jukebox state.
     *
     * @return json
     */
    public function getJukeboxState()
    {
        $response = new JukeboxStateResponse();
        $response->savedTracks = $this->getJsonListTracks();
        $response->queuedTracks = $this->getJsonQueuedTracks();
        $response->nowPlaying = $this->getJsonLoadedTrack();
        self::jsonResponse(true, $response);
    }

    /**
     * API Endpoint - Reset Jukebox.
     *
     * @return json
     */
    public function reset()
    {
        $this->createJukebox();
        self::jsonResponse(true);
    }



    /**
     * Return a Json response.
     *
     * @return json
     */
    private static function jsonResponse($success, $data = null, $message = null)
    {
        header('Content-Type: application/json');
        echo json_encode(new JukeboxApiResponse($success, $data, $message));
    }


    /**
     * Get Json friendly model of tracks.
     *
     * @return array
     */
    private static function getJsonTracks($tracks)
    {
        $trackData = array();
        foreach ($tracks as $track)
        {
            $trackData[] = $track->getTrackViewModel();
        }
        return $trackData;
    }

    /**
     * Get Jukebox, create
     * if none exists in PHP session.
     *
     * @return Jukebox
     */
    private function getJukebox() 
    {
        // Attempt to get from session
        $jukebox = $this->getJukeboxFromSession();

        if (!$jukebox || !($jukebox instanceof Jukebox))
        {            
            $jukebox = $this->createJukebox();
        }

        return $jukebox;
    }

    /**
     * Get Jukebox from PHP session.
     *
     * @return Jukebox
     */
    private function getJukeboxFromSession() 
    {
        if (isset($_SESSION["jukebox"]))
        {
            return $_SESSION["jukebox"];
        }
        return null;
    }


    /**
     * Get create new Jukebox and add to PHP session.
     *
     * @return Jukebox
     */
    private function createJukebox() 
    {
        $jukebox = new Jukebox(self::getPreAddedTracks());
        $_SESSION["jukebox"] = $jukebox;
        return $jukebox;
    }

    private static function getPreAddedTracks()
    {
        return array(
            new SoundcloudTrack("A Little Soul - Smoove remix", "https://soundcloud.com/stevenmoove/a-little-soul-smoove-suonho"),
            new SpotifyTrack("Leader - Yarah Bravo", "spotify:track:2dWF6LTrIHDdAKdeqMFEA7"),
            new SoundcloudTrack("What's goin on - Marvin Gaye/Paul Nice", "https://soundcloud.com/paul-nice-631571899/whats-goin-on-paulies-potown-reflip"),
            new SoundcloudTrack("Jump around - A Skillz remix", "https://soundcloud.com/a-skillz/jump-around-askillz-re-edit"),
            new SpotifyTrack("C.L. Smooth Unplugged", "https://open.spotify.com/track/6LMQdhAdPpso0kZ7VHT161"),
            new SoundcloudTrack("Return of the beats - Smoove", "https://soundcloud.com/stevenmoove/smoove-return-of-the-beats"),
        );
    }

    /**
     * Process API error.
     * 
     * @return void
     */
    public static function processError($ex)
    {
        $message = "Error! ";
        if ($ex) $message .= $ex->getMessage();
        return self::jsonResponse(false, null, $message);
    }
}



