<?php 

abstract class BaseController 
{
    protected $config;
    private $baseUrl;
    private $basePath = '';


    /**
     * Create a new BaseController instance.
     *
     * @param array $config
     * @return void
     */
    function __construct($config) 
    {
        $this->setConfig($config);

        // Set site's base URL
        $this->baseUrl = 'http://' . $_SERVER['HTTP_HOST'] . $this->basePath;
    }


    /**
     * Process a controller error.
     * 
     * @return void
     */
    abstract public static function processError($ex);


    /**
     * Set routing config.
     *
     * @param array $config
     * @return void
     */
    private function setConfig($config)
    {
        // Store config
        $this->config = $config;
        if (!$config || !is_array($config)) return;

        // Set routing
        if (isset($config['basePath'])) $this->basePath = $config['basePath'];
    }


    /**
     * Super basic templating.
     *
     * @param string $templatePath
     * @param object $pageVars
     * @return void
     */
    protected function view($templatePath, $pageVars)
    {
        // Load template
        require_once('../app/views/' . $templatePath . '.php');
    }


    /**
     * Get formatted path.
     *
     * @param string $path
     * @return string
     */
    protected function getLinkPath($path = null) 
    {
        $linkPath = $this->baseUrl;

        if (!$path) return $linkPath;

        $pathElements = explode($path, '/');
        $controller = $pathElements[0];
        $action = isset($pathElements[1]) ? $pathElements[1] : "";
        $linkPath += '?controller=' . $controller . '&action=' . $action;

        return $linkPath;    
    }


    /**
     * Do relative HTTP redirect.
     *
     * @param string $redirectPath
     * @return void
     */
    protected function doRedirect($redirectPath = '') 
    {
        $redirectPath = $this->getLinkPath($redirectPath);
        header('Location: ' . $redirectPath);
    }

}