<?php 

class ErrorController extends BaseController
{
    /**
     * Create a new ErrorController instance.
     *
     * @param array $config
     * @return void
     */
    function __construct($config) 
    {
        parent::__construct($config);
    }


    /**
     * Default action, show error screen.
     *
     * @return void
     */
    public function index() 
    {
        // Get together variables to pass to view
        $pageVars = (object) array(
            'linkPaths' => (object) array(
                'home' => $this->getLinkPath()
            )
        );

        // Show view
        $this->view('error/index', $pageVars);
    }


    /**
     * Process controller error.
     * 
     * @return void
     */
    public static function processError($ex)
    {
    }
}
