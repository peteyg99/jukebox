<?php

// Load libs
require_once 'bootstrap.php';

// Preload api files
$foldersToPreload = array(
    'controllers',
    'models'
);

foreach ($foldersToPreload as $folderName)
{
    $directoryPath = __DIR__ . '/' . $folderName;
    foreach (scandir($directoryPath) as $filename) 
    {
        $path = $directoryPath . '/' . $filename;
        if (is_file($path)) {
            require_once $path;
        }
    }
}
