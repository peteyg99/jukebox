<?php

abstract class StorageDevice
{
    private $items = array();

    /**
     * Return device's author.
     *
     * @return string
     */
    abstract public static function getAuthor();

    /**
     * Return current time.
     *
     * @return string
     */
    public static function getTimeDisplay()
    {
        $currentTime = new DateTime();
        return $currentTime->format("H:i");
    }

    /**
     * Return item from device by itemId.
     *
     * @param int $itemId
     * @return Item
     */
    protected function getItem($itemId)
    {
        if (!$this->itemExists($itemId)) throw new Exception("Cannot get item, does not exist");
        return $this->items[$itemId];
    }

    /**
     * Return items on device.
     *
     * @return array
     */
    protected function getItems()
    {
        return $this->items;
    }

    /**
     * Add a item to device.
     *
     * @param Item $item
     * @return void
     */
    protected function addItem($item)
    {
        if (!$item instanceof Item) throw new Exception("Invalid item");
        $newItemId = $this->calculateNewItemId();
        $item->setItemId($newItemId);
        $this->items[$newItemId] = $item;
    }

    /**
     * Calculate a new itemId.
     *
     * @return int
     */
    private function calculateNewItemId()
    {
        end($this->items);
        return key($this->items) + 1;
    }

    /**
     * Add multiple items to device.
     *
     * @param array $items
     * @return void
     */
    protected function addItems($items)
    {
        if (!is_array($items)) throw new Exception("Not an array of items");

        foreach ($items as $item)
        {
            $this->addItem($item);
        }
    }

    /**
     * Remove an item from device by itemId.
     *
     * @param int $itemId
     * @return void
     */
    protected function removeItem($itemId)
    {
        if (!$this->itemExists($itemId)) throw new Exception("Cannot remove item, does not exist");
        unset($this->items[$itemId]);
    }

    /**
     * Check an item exists by itemId.
     *
     * @param int $itemId
     * @return bool
     */
    protected function itemExists($itemId)
    {
        return isset($this->items[$itemId]);
    }

}