<?php

class SoundcloudTrack extends Track
{
    protected $playUrl = "https://w.soundcloud.com/player/?url={tracklink}&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true";
    protected $logoUrl = "https://i1.sndcdn.com/artworks-000039155469-vbsgpa-t500x500.jpg";
}
