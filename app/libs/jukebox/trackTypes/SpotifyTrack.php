<?php

class SpotifyTrack extends Track
{
    protected $playUrl = "https://embed.spotify.com/?uri={tracklink}";
    protected $logoUrl = "https://spotify.i.lithium.com/t5/image/serverpage/image-id/6482i7BC0665B52558F27/image-size/small?v=mpbl-1&px=-1";
}