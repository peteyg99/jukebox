<?php

require_once 'StorageDevice.php';
require_once 'Track.php';

class MusicStorageDevice extends StorageDevice
{
    /**
     * Return device's author.
     *
     * @return string
     */
    public static function getAuthor()
    {
        return "Pete Gaulton";
    }

    /**
     * List available tracks.
     *
     * @return array
     */
    public function listTracks()
    {
        return $this->getItems();
    }

    /**
     * Add new track to device.
     *
     * @param Track $track
     * @return void
     */
    public function addTrack($track)
    {
        if (!$track instanceof Track) throw new Exception("Invalid track");
        $this->addItem($track);
    }

    /**
     * Remove track from device.
     *
     * @param int $trackId
     * @return void
     */
    public function removeTrack($trackId)
    {
        $this->removeItem($trackId);
    }
}