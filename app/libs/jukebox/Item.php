<?php

abstract class Item
{
    private $itemId;

    /**
     * Set Item's itemId.
     *
     * @param int $itemId
     * @return void
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    }

    /**
     * Get Item's itemId.
     *
     * @return int
     */
    public function getItemId()
    {
        return $this->itemId;
    }
}