<?php

require_once 'Item.php';
require_once 'TrackViewModel.php';

abstract class Track extends Item
{
    private $name;
    protected $link;

    protected $embedMarkup = '<iframe src="{playUrl}" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>';

    // Properties to be set on inherited class
    protected $playUrl;
    protected $logoUrl;

    /**
     * Create a new Track instance.
     *
     * @return void
     */
    function __construct($name, $link)
    {
        $this->name = $name;
        $this->link = $link;
    }   

    /**
     * Get Track's playUrl.
     *
     * @return string
     */
    public function getPlayUrl()
    {
        if (!$this->playUrl) throw new Exception("playUrl is not set for this track type");
        return str_replace("{tracklink}", $this->link, $this->playUrl);
    }

    /**
     * Get Track's embed markup.
     *
     * @return string
     */
    public function getEmbed()
    {
        $playUrl = $this->getPlayUrl();
        return str_replace("{playUrl}", $playUrl, $this->embedMarkup);
    }

    /**
     * Get Track's logoUrl.
     *
     * @return string
     */
    public function getLogoUrl()
    {
        if (!$this->logoUrl) throw new Exception("logoUrl is not set for this track type");
        return $this->logoUrl;
    }
   
    /**
     * Get Track's name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Get Track's link.
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
    
    /**
     * Get Track's trackId.
     *
     * @return string
     */
    public function getTrackId()
    {
        return $this->getItemId();
    }

    /**
     * Get track model for Json response.
     *
     * @return TrackViewModel
     */
    public function getTrackViewModel()
    {
        return new TrackViewModel($this);
    }

}