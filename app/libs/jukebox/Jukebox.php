<?php

require_once 'MusicStorageDevice.php';
require_once 'trackTypes/SoundcloudTrack.php';
require_once 'trackTypes/SpotifyTrack.php';

class Jukebox extends MusicStorageDevice
{
    public $player;
    private $queuedTracks = array();

    /**
     * Create a new Jukebox instance.
     *
     * @return void
     */
    function __construct($preAddedTracks = array())
    {
        // Add tracks
        foreach ($preAddedTracks as $track) {
            $this->addTrack($track);        
        }
    }

    /**
     * Queue track on jukebox.
     *
     * @param int $trackId
     * @return void
     */
    public function queueTrack($trackId)
    {
        $this->queuedTracks[] = $this->getItem($trackId);
    }    

    /**
     * Get list of queued tracks.
     *
     * @return array
     */
    public function getQueuedTracks()
    {
        return $this->queuedTracks;
    }    

    /**
     * Get loaded track from jukebox (ie. top of queue).
     *
     * @return Track
     */
    public function getLoadedTrack()
    {
        $queuedTracks = $this->getQueuedTracks();
        $loadedTrack = array_shift($queuedTracks);
        if ($loadedTrack && !$loadedTrack instanceof Track) throw new Exception("Invalid track");
        return $loadedTrack;
    }    

    /**
     * Skip to next track in queue.
     *
     * @return void
     */
    public function skipToNextQueuedTrack()
    {
        array_shift($this->queuedTracks);
    }    
    
}