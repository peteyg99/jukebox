<?php

class TrackViewModel 
{
    public $name;
    public $trackId;
    public $embed;
    public $embedUrl;
    public $logo;

    /**
     * Create a new TrackViewModel instance.
     *
     * @return void
     */
    function __construct($track)
    {
        $this->name = $track->getName();
        $this->trackId = $track->getTrackId();
        $this->embed = $track->getEmbed();
        $this->embedUrl = $track->getPlayUrl();
        $this->logo = $track->getLogoUrl();
    }   
}

