<?php

class JukeboxTest extends Jukebox_TestBase
{
    public function setUp()
    {
        $this->getPreAddedTracks();
        $this->jukebox = $this->getMockForAbstractClass('Jukebox', array($this->preAddedTracks));
    }

    public function testCanCreateJukebox()
    {
        $this->assertNotNull($this->jukebox);
        $this->assertInstanceOf(Jukebox::class, $this->jukebox);
    }

    public function testCreatedJukeboxHasPreAddedTracksTracks()
    {
        $this->do_testCreatedJukeboxHasPreAddedTracksTracks();
    }


    public function testJukeboxCanQueueTrack()
    {
        $tracks = $this->jukebox->listTracks();

        $trackToSelect = reset($tracks);
        $this->assertInstanceOf(Track::class, $trackToSelect);

        $this->jukebox->queueTrack($trackToSelect->getItemId());
        $queuedTrack = $this->jukebox->getLoadedTrack();
        $this->assertInstanceOf(Track::class, $queuedTrack);

        $this->assertEquals($trackToSelect, $queuedTrack);
    }

    public function testJukeboxCanQueueMultipleTracks()
    {
        $tracks = $this->jukebox->listTracks();

        $trackToSelect1 = array_shift($tracks);
        $this->assertInstanceOf(Track::class, $trackToSelect1);
        $trackToSelect2 = array_shift($tracks);
        $this->assertInstanceOf(Track::class, $trackToSelect2);

        $this->assertNotEquals($trackToSelect1, $trackToSelect2);

        $this->jukebox->queueTrack($trackToSelect1->getItemId());
        $this->jukebox->queueTrack($trackToSelect2->getItemId());

        $queuedTracks = $this->jukebox->getQueuedTracks();
        $this->assertGreaterThan(1, $queuedTracks);

        $this->assertContains($trackToSelect1, $queuedTracks);
        $this->assertContains($trackToSelect2, $queuedTracks);

        return $this->jukebox;
    }

  

    /**
     * @depends testJukeboxCanQueueMultipleTracks
     */
    public function testLoadedTrackIsFirstInQueue()
    {
        // set jukebox from previous test
        $this->jukebox = func_get_args()[0];

        $queuedTracks = $this->jukebox->getQueuedTracks();
        $this->assertGreaterThan(1, $queuedTracks);

        $firstInQueue = array_shift($queuedTracks);

        $loadedTrack = $this->jukebox->getLoadedTrack();

        $this->assertEquals($firstInQueue, $loadedTrack);
    }

    /**
     * @depends testJukeboxCanQueueMultipleTracks
     */
    public function testJukeboxCanSkipToNextQueuedTrack()
    {
        // set jukebox from previous test
        $this->jukebox = func_get_args()[0];

        $queuedTracks = $this->jukebox->getQueuedTracks();
        $this->assertGreaterThan(1, $queuedTracks);

        $firstInQueue = array_shift($queuedTracks);
        $nextInQueue = array_shift($queuedTracks);

        $loadedTrack = $this->jukebox->getLoadedTrack();
        $this->assertEquals($firstInQueue, $loadedTrack);

        $this->jukebox->skipToNextQueuedTrack();

        $loadedTrack = $this->jukebox->getLoadedTrack();
        $this->assertEquals($nextInQueue, $loadedTrack);
    }


}