<?php

class SoundcloudTrackTest extends TrackType_TestBase
{
    public function setUp()
    {
        $this->track = new SoundcloudTrack($this->name, $this->link);
    }

    public function testCanCreateSoundcloudTrack()
    {
        $this->assertNotNull($this->track);
        $this->assertInstanceOf(SoundcloudTrack::class, $this->track);
    }
}