<?php

class MusicStorageDeviceTest extends MusicStorageDevice_TestBase
{
    public function setUp()
    {
        $this->device = new MusicStorageDevice();
    }

    public function testCanCreateDevice()
    {
        $this->assertNotNull($this->device);
        $this->assertInstanceOf(MusicStorageDevice::class, $this->device);
    }

    public function testCanGetTimeDisplay()
    {
        $timeDisplay = MusicStorageDevice::getTimeDisplay();
        $this->assertInternalType('string', $timeDisplay);
    }

    public function testCanGetDeviceAuthor()
    {
        $author = MusicStorageDevice::getAuthor();
        $this->assertInternalType('string', $author);
    }

    public function testDeviceCanListTracks()
    {
        $tracks = $this->device->listTracks();
        $this->assertInternalType('array', $tracks);
    }

    public function testDeviceCanAddNewTrack()
    {
        $tracksBefore = $this->device->listTracks();

        $this->device->addTrack(self::getNewTrack());

        $tracksAfter = $this->device->listTracks();

        $numAddedTracks = count($tracksAfter) - count($tracksBefore);
        $this->assertEquals(1, $numAddedTracks);

        return $this->device;
    }

    /**
     * @depends testDeviceCanAddNewTrack
     */
    public function testDeviceCanRemoveTrack()
    {
        // set device from previous test
        $this->device = func_get_args()[0];

        $tracksBefore = $this->device->listTracks();

        $trackToRemove = reset($tracksBefore);
        $this->assertInstanceOf(Track::class, $trackToRemove);

        $this->device->removeTrack($trackToRemove->getItemId());

        $tracksAfter = $this->device->listTracks();

        $numRemovedTracks = count($tracksBefore) - count($tracksAfter);
        $this->assertEquals(1, $numRemovedTracks);
    }

}