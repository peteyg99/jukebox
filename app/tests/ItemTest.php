<?php

class ItemTest extends PHPUnit_Framework_TestCase
{
    private $item;

    public function setUp()
    {
        $this->item = $this->getMockForAbstractClass('Item');
    }

    public function testCanCreateItem()
    {
        $this->assertNotNull($this->item);
        $this->assertInstanceOf(Item::class, $this->item);
    }


    private function getItemId()
    {
        $class = new ReflectionClass('Item');
        $prop = $class->getProperty('itemId');
        $prop->setAccessible(true);
        return $prop->getValue($this->item);
    }

    public function testCanSetItemId()
    {
        $itemId = 99;
        $this->item->setItemId($itemId);

        $this->assertEquals($itemId, $this->getItemId());

        return $this->item;
    }

    /**
     * @depends testCanSetItemId
     */
    public function testCanGetItemId()
    {
        // set item from previous test
        $this->device = func_get_args()[0];

        $itemId = $this->item->getItemId();

        $this->assertEquals($this->getItemId(), $itemId);
    }

}