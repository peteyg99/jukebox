<?php

date_default_timezone_set('UTC');

class StorageDeviceTest extends PHPUnit_Framework_TestCase
{
    private $device;
    private $item;

    public function setUp()
    {
        $this->device = $this->getMockForAbstractClass('StorageDevice');
        $this->item = $this->getMockForAbstractClass('Item');
    }

    private static function getMethod($name) {
        $class = new ReflectionClass('StorageDevice');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    private function invokeMethod($name, $args = null) {
        return self::getMethod($name)->invokeArgs($this->device, array($args));
    }

    public function testCanCreateDevice()
    {
        $this->assertNotNull($this->device);
        $this->assertInstanceOf(StorageDevice::class, $this->device);
    }

    public function testCanGetTimeDisplay()
    {
        $timeDisplay = StorageDevice::getTimeDisplay();
        $this->assertInternalType('string', $timeDisplay);
    }

    public function testDeviceCanReturnItems()
    {
        $items = $this->invokeMethod('getItems');
        $this->assertInternalType('array', $items);
    }

    public function testDeviceCanAddItem()
    {
        $itemsBefore = $this->invokeMethod('getItems');

        $this->invokeMethod('addItem', $this->item);

        $itemsAfter = $this->invokeMethod('getItems');

        $numAddedItems = count($itemsAfter) - count($itemsBefore);
        $this->assertEquals(1, $numAddedItems);
    }

    public function testDeviceCanAddMultipleItems()
    {
        $itemsBefore = $this->invokeMethod('getItems');

        $items = array($this->item, $this->item);
        $this->invokeMethod('addItems', $items);

        $itemsAfter = $this->invokeMethod('getItems');

        $numAddedItems = count($itemsAfter) - count($itemsBefore);
        $this->assertEquals(2, $numAddedItems);

        return $this->device;
    }

    /**
     * @depends testDeviceCanAddMultipleItems
     */
    public function testDeviceCanReturnItem()
    {
        // set device from previous test
        $this->device = func_get_args()[0];

        $items = $this->invokeMethod('getItems');

        $itemToSelect = reset($items);
        $this->assertInstanceOf(Item::class, $itemToSelect);

        $selectedItem = $this->invokeMethod('getItem', $itemToSelect->getItemId());
        $this->assertInstanceOf(Item::class, $selectedItem);

        $this->assertEquals($itemToSelect, $selectedItem);
    }    

    /**
     * @depends testDeviceCanAddMultipleItems
     */
    public function testDeviceCanRemoveItem()
    {
        // set device from previous test
        $this->device = func_get_args()[0];

        $itemsBefore = $this->invokeMethod('getItems');

        $itemToRemove = reset($itemsBefore);
        $this->assertInstanceOf(Item::class, $itemToRemove);

        $this->invokeMethod('removeItem', $itemToRemove->getItemId());

        $itemsAfter = $this->invokeMethod('getItems');

        $numRemovedItems = count($itemsBefore) - count($itemsAfter);
        $this->assertEquals(1, $numRemovedItems);
    }

}