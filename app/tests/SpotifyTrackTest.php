<?php

class SpotifyTrackTest extends TrackType_TestBase
{
    public function setUp()
    {
        $this->track = new SpotifyTrack($this->name, $this->link);
    }

    public function testCanCreateSpotifyTrack()
    {
        $this->assertNotNull($this->track);
        $this->assertInstanceOf(SpotifyTrack::class, $this->track);
    }
}