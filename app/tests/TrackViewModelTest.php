<?php

class TrackViewModelTest extends Track_TestBase
{
    private $trackViewModel;

    public function setUp()
    {
        $this->track = $this->getMockForAbstractClass('Track', array($this->name, $this->link));
        $this->setPlayUrl();
        $this->setLogoUrl();
        $this->trackViewModel = new TrackViewModel($this->track);
    }

    public function testPropertiesAreSetFromConstructor()
    {
        $this->assertEquals($this->track->getName(), $this->trackViewModel->name);
        $this->assertEquals($this->track->getTrackId(), $this->trackViewModel->trackId);
        $this->assertEquals($this->track->getEmbed(), $this->trackViewModel->embed);
        $this->assertEquals($this->track->getPlayUrl(), $this->trackViewModel->embedUrl);
        $this->assertEquals($this->track->getLogoUrl(), $this->trackViewModel->logo);
    }

}