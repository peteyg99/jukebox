<?php

class TrackTest extends Track_TestBase
{
    public function setUp()
    {
        $this->track = $this->getMockForAbstractClass('Track', array($this->name, $this->link));
    }

    public function testCanCreateTrack()
    {
        $this->assertNotNull($this->track);
        $this->assertInstanceOf(Track::class, $this->track);
    }

    public function testPropertiesAreSetFromConstructor()
    {
        $this->assertEquals($this->name, $this->getPropertyValue('name'));
        $this->assertEquals($this->link, $this->getPropertyValue('link'));
    }

    /**
     * @expectedException Exception
     */
    public function testGetPlayUrlThrowsExeptionIfValueNotSet()
    {
        $this->invokeMethod('getPlayUrl');
    }

    public function testCanGetPlayUrlIfValueSet()
    {
        $this->setPlayUrl();
        $fullPlayUrl = $this->invokeMethod('getPlayUrl');

        $this->assertContains($this->link, $fullPlayUrl);
    }
    
    /**
     * @expectedException Exception
     */
    public function testGetEmbedThrowsExeptionIfValueNotSet()
    {
        $this->track->getEmbed();
    }

    public function testCanGetEmbedIfValueSet()
    {
        $this->setPlayUrl();
        $embed = $this->track->getEmbed();
        $fullPlayUrl = $this->invokeMethod('getPlayUrl');

        $this->assertContains($fullPlayUrl, $embed);
    }

    /**
     * @expectedException Exception
     */
    public function testGetLogoUrlThrowsExeptionIfValueNotSet()
    {
        $this->track->getLogoUrl();
    }

    public function testCanGetLogoUrlIfValueSet()
    {
        $this->setLogoUrl();
        $logo = $this->track->getLogoUrl();

        $this->assertContains($this->logoUrl, $logo);
    }


    public function testCanGetName()
    {
        $name = $this->track->getName();
        $this->assertEquals($this->name, $name);
    }

    public function testCanGetLink()
    {
        $link = $this->track->getLink();
        $this->assertEquals($this->link, $link);
    }

    public function testCanGetJsonModel()
    {
        $this->setPlayUrl();
        $this->setLogoUrl();
        $jsonModel = $this->track->getTrackViewModel();
        $this->assertInstanceOf(TrackViewModel::class, $jsonModel);

        $this->assertEquals($this->track->getName(), $jsonModel->name);
        $this->assertEquals($this->track->getEmbed(), $jsonModel->embed);
        $this->assertEquals($this->track->getLogoUrl(), $jsonModel->logo);
    }
}