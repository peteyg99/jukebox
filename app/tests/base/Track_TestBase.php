<?php

abstract class Track_TestBase extends PHPUnit_Framework_TestCase
{
    protected $track;
    protected $name = "Test track";
    protected $link = "http://www.test.com";
    protected $playUrl = "testPlayUrl{tracklink}";
    protected $logoUrl = "testLogoUrl";

    protected static function getReflectionClass()
    {
        return new ReflectionClass('Track');
    }

    protected static function getReflectionProperty($propName)
    {
        $class = self::getReflectionClass();
        $prop = $class->getProperty($propName);
        $prop->setAccessible(true);
        return $prop;
    }

    protected function getPropertyValue($propName)
    {
        $prop = self::getReflectionProperty($propName);
        return $prop->getValue($this->track);
    }

    protected function setPropertyValue($propName, $value)
    {
        $prop = self::getReflectionProperty($propName);
        return $prop->setValue($this->track, $value);
    }

    protected static function getReflectionMethod($methodName)
    {
        $class = self::getReflectionClass();
        $method = $class->getMethod($methodName);
        $method->setAccessible(true);
        return $method;
    }

    protected function invokeMethod($methodName, $args = null) {
        return self::getReflectionMethod($methodName)->invokeArgs($this->track, array($args));
    }

    protected function setPlayUrl()
    {
        $this->setPropertyValue('playUrl', $this->playUrl);
    }

    protected function setLogoUrl()
    {
        $this->setPropertyValue('logoUrl', $this->logoUrl);
    }
}