<?php

abstract class MusicStorageDevice_TestBase extends PHPUnit_Framework_TestCase
{
    protected $device;

    protected function getNewTrack()
    {
        return $this->getMockForAbstractClass('Track', array("Test track", "http://www.test.com"));
    }
}

abstract class Jukebox_TestBase extends MusicStorageDevice_TestBase
{
    protected $jukebox;
    protected $preAddedTracks;

    protected function getPreAddedTracks()
    {
        $this->preAddedTracks = array(
            $this->getNewTrack(),
            $this->getNewTrack(),
            $this->getNewTrack(),
            $this->getNewTrack(),
        );
    }

    protected function do_testCreatedJukeboxHasPreAddedTracksTracks()
    {
        $tracks = $this->jukebox->listTracks();
        $this->assertGreaterThan(0, count($tracks));
        $this->assertEquals(count($this->preAddedTracks), count($tracks));
    }
}