<?php

abstract class TrackType_TestBase extends Track_TestBase
{
    public function testCanGetPlayUrl()
    {
        $fullPlayUrl = $this->track->getPlayUrl();

        $this->assertContains($this->link, $fullPlayUrl);
    }

    public function testCanGetEmbed()
    {
        $embed = $this->track->getEmbed();
        $fullPlayUrl = $this->track->getPlayUrl();

        $this->assertContains($fullPlayUrl, $embed);
    }

    public function testCanGetLogoUrl()
    {
        $logoUrl = $this->track->getLogoUrl();
        $testLogoUrl = $this->getPropertyValue('logoUrl');

        $this->assertEquals($testLogoUrl, $logoUrl);
    }
}