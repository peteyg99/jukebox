<?php

require_once('autoload.php');


function dashesToCamelCase($string, $capitalizeFirstCharacter = false) 
{
    $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    if (!$capitalizeFirstCharacter) {
        $str[0] = strtolower($str[0]);
    }
    return $str;
}


// Routing 
if (isset($_GET['controller'])) {
    $controllerName = $_GET['controller'];
} else {
    $controllerName = 'home';
}
if (isset($_GET['action'])) {
    $action = dashesToCamelCase($_GET['action']);
} else {
    $action = 'index';
}


// Get controller
$config = parse_ini_file('config.ini');
switch ($controllerName)
{
    case 'error':
        $controller = new ErrorController($config);
        break;
    case 'api':
        $controller = new JukeboxApiController($config);
        break;
    default:
        $controller = new HomeController($config);
    
}


// Load action
if (method_exists($controller, $action))
{
    try
    {
        $controller->$action();
    }
    catch (Exception $ex)
    {
        $controller::processError($ex);
    }
}
else
{
    $errorMsg = "Action does not exist on controller:" . $controllerName;
    $controller::processError(new Exception($errorMsg));
}
