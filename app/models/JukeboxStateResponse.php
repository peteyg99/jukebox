<?php

class JukeboxStateResponse
{
    public $savedTracks;
    public $queuedTracks;
    public $nowPlaying;
}