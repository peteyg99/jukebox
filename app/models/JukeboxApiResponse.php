<?php

class JukeboxApiResponse
{
    public $success = false;
    public $data;
    public $message;

    function __construct($success, $data = null, $message = null) 
    {
        $this->success = $success;
        $this->data = $data;
        $this->message = $message;
    }
}
