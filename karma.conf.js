module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['mocha', 'browserify'],
    files: [
      'app/assets/tests/*.js'
    ],
    exclude: [
    //   'server.js'
    ],
    browserify: {
      extensions: ['.js', '.jsx'],
      debug: true,
      transform: [
        ['babelify',
        {
          presets: ['es2015', 'react'],
          plugins: ['transform-object-rest-spread']
        }]
      ],
      configure: function(bundle) {
        bundle.on('prebundle', function() {
          bundle.external('react/addons');
          bundle.external('react/lib/ReactContext');
          bundle.external('react/lib/ExecutionEnvironment');
        });
      }
    },
    preprocessors: {
      'app/assets/tests/*.js': ['browserify']
    },
    reporters: ['progress', 'mocha'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    concurrency: Infinity
  })
}